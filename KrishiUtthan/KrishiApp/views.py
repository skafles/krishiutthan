# Create your views here.
from KrishiApp.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
import datetime
import operator


def show_price(request):
    """This method returns price of items"""
    parameters= {}
    date_today = datetime.datetime.now().date()
    item= Item.manager.all()
    items = []
    for each_item in item:
        price=Price.manager.filter(item=each_item, date=date_today)
        price=list(price)
        price.sort(key = operator.attrgetter('user_price'))
        items.append(price[0])
        # print price[0].user_price
        # print price[1].user_price

    
    parameters["items"] = items
    return render_to_response('home.html',parameters)
    
def item_page(request, item_id):
    """This method returns list of seller, price and location for an item"""
    parameters= {}
    date_today = datetime.datetime.now().date()
    item= Item.manager.get(id=item_id)
    items = []
    price=Price.manager.filter(item=item, date=date_today)
    price=list(price)
    price.sort(key = operator.attrgetter('farmer_price'))
    #print len(price)
    for i in range(len(price)):
        items.append(price[i])
    #items.append(price[0])
        # print price[0].user_price
        # print price[1].user_price    
    #print items
    parameters["items"] = items
    parameters["name"] = item.name
    parameters["category"] = item.category
    return render_to_response('items.html', parameters)


def seller_page(request, seller_id):
    """This method returns list of item and price for a vendor"""
    parameters= {}
    date_today = datetime.datetime.now().date()
    item= Item.manager.all()
    items = []
    seller=Seller.manager.get(id=seller_id)
    print seller.name
    for each_item in item:
        price=Price.manager.filter(item=each_item, date=date_today, seller=seller)
        price=list(price)

        if price!=[]:
            items.append(price[0])
        
    parameters["items"] = items
    parameters["name"] = seller.name
    parameters["phone"] = seller.phone
    parameters["location"] = seller.district.name
    return render_to_response('seller.html', parameters)

def test(request):
    return render_to_response('base2.html')