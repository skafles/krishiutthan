# Create your models here.
from django.db import models
from KrishiApp.manager import *

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length = 50)
    manager = CategoryManager()

    def __unicode__(self):
        return self.name

class District(models.Model):
    name = models.CharField(max_length = 50)
    manager = DistrictManager()

    def __unicode__(self):
        return self.name


class Seller(models.Model):
    """stores the info of seller"""
    key = models.CharField(max_length = 8)
    name = models.CharField(max_length = 100, blank = True)
    phone = models.CharField(max_length = 100, blank = True)
    district = models.ForeignKey(District, blank = True, null= True)
    manager = SellerManager()

    def __unicode__(self):
        return self.name

    
class Item(models.Model):
    """stores the details of agricultural item"""
    name = models.CharField(max_length = 100, blank = True)
    category = models.ForeignKey(Category, blank = True, null= True)
    manager = ItemManager()

    def __unicode__(self):
        return self.name


class Price(models.Model):
    """stores the price of agricultural products """
    user_price = models.CharField(max_length = 100, blank = True)
    farmer_price = models.CharField(max_length = 100, blank = True)
    date = models.DateField()
    item = models.ForeignKey(Item, blank = True, null= True)
    seller = models.ForeignKey(Seller, blank = True, null= True)
    manager = PriceManager()

    def __unicode__(self):
        return self.item.name+" "+str(self.date)+" "+self.seller.name
    