from django.db import models


class CategoryManager(models.Manager):
    pass


class DistrictManager(models.Manager):
    pass


class SellerManager(models.Manager):
    pass


class ItemManager(models.Manager):
    pass


class PriceManager(models.Manager):
    pass
