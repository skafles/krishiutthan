from django.http import HttpResponse, HttpResponseRedirect
#import urllib
from django.views.decorators.csrf import csrf_exempt
from KrishiApp.models import *
import datetime
import operator
from django.shortcuts import render_to_response


def return_price(item_name, total_return, order, district):
    items = []
    date_today = datetime.datetime.now().date()
    print item_name
    item = Item.manager.get(name=item_name)
    price = Price.manager.filter(date=date_today, item=item)
    price = list(price)
    
    if district !="none":
        new_price = []
        for each_item in price:
            if each_item.seller.district.name == district:
                new_price.append(each_item)
        price = new_price
        
    price.sort(key=operator.attrgetter('farmer_price'), reverse=True)
    items = list(price)
    return items


def get_price(item):
    sp_text = item.split(" ")
    sp_item = sp_text[0]
    sp_reply = "Price per kg for " + sp_item
    if len(sp_text)==1:
        district ="none"
    else:
        district = sp_text[1]
        sp_reply += " in "+ district
    highest_price = return_price(sp_item, 3, "desc", district )
   
    entered_phone = []
    for each_item in highest_price:
        if entered_phone.count(each_item.seller.phone)==0:
            sp_reply += "\r\n" + str(each_item.seller.phone) + " -> Rs." + str(each_item.farmer_price) +""
            entered_phone.append(each_item.seller.phone)

    #sp_reply = urllib.quote(sp_reply)
    return sp_reply

@csrf_exempt
def get_vendor(given_text):
    sp_text = given_text.lower()

    vender_id = int(sp_text)
    seller = Seller.manager.get(phone=vender_id)
    sp_reply =""
    sp_reply += "Seller Name: "+ str(seller.name)
    sp_reply += " District: " + str(seller.district)
    sp_reply += " Phone " + str(seller.phone)
    # except:
    #     sp_reply = "Sorry, Error in your vendor id"

    return sp_reply

@csrf_exempt
def update_price(given_text):
    sp_text = given_text
    sp_text = sp_text.split(" ")
    
    vendor_id = int(sp_text[0])
    seller_key = sp_text[1]
    item = sp_text[2]
    new_price = int(sp_text[3])

    print vendor_id, seller_key, item, new_price
    seller = Seller.manager.get(id=vendor_id)
    
    if seller.key != seller_key:
        msg ="Sorry wrong seller key"
        return msg
    item = Item.manager.get(name = item.lower())
    
    date = datetime.datetime.now().date()
    price = Price()
    price.farmer_price = new_price
    price.seller = seller
    price.date = date
    price.item = item
    price.save()
    sp_reply = "Updated succesffully"    

    # except:
    #     sp_reply = "Sorry, There is error in your parameters"
    return sp_reply


@csrf_exempt
def get_request(request):
    sp_keyword = request.GET['keyword']
    sp_from = request.GET['from']
        
    sp_text = request.GET['text']
    print sp_keyword.lower()
    msg = ""
    if sp_keyword.lower() == "price":
        print "entered"
        print sp_text
        msg = get_price(sp_text)
    if sp_keyword.lower() == "seller":
        msg = get_vendor(sp_text)
    if sp_keyword == "update":
        msg = update_price(sp_text)
    # except:
    #     msg = "Sorry you have not used proper format"
    #     pass

    return HttpResponse(msg)
